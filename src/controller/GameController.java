package controller;

import model.AdjacentSquareCalculator;
import model.GameLogic;
import view.UserInput;

public class GameController 
{
	
	GameLogic gl;
	UserInput ui;
	AdjacentSquareCalculator asc;
	
	
	public GameController(String[] args)
	{
		ui = new UserInput();
		ui.validate_board_dimensions(args);
		ui.validate_user_board();
		asc = new AdjacentSquareCalculator(ui.getBoard_height(), ui.getBoard_width(), ui.getUser_board_input());
		gl = new GameLogic(asc); 
		gl.setAllSafeSquareNumbers();
		gl.printOutput();
	}

}
