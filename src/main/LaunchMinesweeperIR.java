package main;

import controller.GameController;

public class LaunchMinesweeperIR {

	//This is a test to make sure that the remote
	// is up to date
	
	public static void main(String[] args) {
	
		//We first need to count the number of arguments.
		if (args.length != 2)
		{ 
			System.out.println("Error: Please enter 2 arguments");
			System.out.println("Format: [Number of Lines] [Number of Columns]");
		}
		else
		{
			//System.out.println("2 Arguments Entered");
			
			new GameController(args);
			
		}
		
	}

}
