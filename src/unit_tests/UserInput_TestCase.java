package unit_tests;

import static org.junit.Assert.*;

import java.util.regex.Pattern;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

public class UserInput_TestCase {

	@Rule
	public final SystemOutRule sor = new SystemOutRule();
	
	
	@Test
	public void test_invalid_board_height_command_line_input()
	{
		
		int board_width;
		int board_height;	
	
		String args[] = {"a","2"};
		//First enable the system log
		sor.enableLog();
		try
		{
			board_height = Integer.parseInt(args[0]);
			board_width = Integer.parseInt(args[1]);
			
			if (board_height <= 0 || board_width <= 0 || board_width >= 100)
			{
				throw new Exception();
			}
		}
		catch(Exception e)
		{
			System.out.println("One or more of the arguments passed in was invalid.");	
		}		

		//Write the assertion statement
		assertEquals(sor.getLog(),"One or more of the arguments passed in was invalid.\n");
		//Now we clear the log
		sor.clearLog();
	}
	
	@Test
	public void test_invalid_board_width_command_line_input()
	{
		
		int board_width;
		int board_height;	
	
		String args[] = {"2","a"};
		//First enable the system log
		sor.enableLog();
		try
		{
			board_height = Integer.parseInt(args[0]);
			board_width = Integer.parseInt(args[1]);
			
			if (board_height <= 0 || board_width <= 0 || board_width >= 100)
			{
				throw new Exception();
			}
		}
		catch(Exception e)
		{
			System.out.println("One or more of the arguments passed in was invalid.");	
		}		

		//Write the assertion statement
		assertEquals(sor.getLog(),"One or more of the arguments passed in was invalid.\n");
		//Now we clear the log
		sor.clearLog();
	}
	
	
//	@Test
//	public void test_invalid_board_width()
//	{
//		
//		int board_width = 0;
//	
//		if (board_width <= 0 || board_width >= 100)
//		{
//			System.out.println("Please enter a valid board width:");
//		}
//		
//	}
	
	@Test
	public void test_regex_expected_true()
	{
		//This regex expression searches the string for all 
		//characters apart from (*) and (.)
		Pattern regex_pattern = Pattern.compile("[^\\*\\.]");
		String temp_line = "....l";
		String temp_line2 = "****l";
		String temp_line3 = "**l..";
		//We expect this to be true because of the 'l' character
		//in the strings.
		assertTrue(regex_pattern.matcher(temp_line).find());
		assertTrue(regex_pattern.matcher(temp_line2).find());
		assertTrue(regex_pattern.matcher(temp_line3).find());
	
	}
	
	@Test
	public void test_regex_expected_false()
	{
		//This regex expression searches the string for all 
		//characters apart from (*) and (.)
		Pattern regex_pattern = Pattern.compile("[^\\*\\.]");
		String temp_line = ".....";
		String temp_line2 = "*****";
		String temp_line3 = "...**";
		//We expect this to be false.
		assertFalse(regex_pattern.matcher(temp_line).find());
		assertFalse(regex_pattern.matcher(temp_line2).find());
		assertFalse(regex_pattern.matcher(temp_line3).find());
	
	}

}
