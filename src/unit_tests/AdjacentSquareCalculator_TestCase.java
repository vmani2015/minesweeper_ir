package unit_tests;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import model.AdjacentSquareCalculator;

public class AdjacentSquareCalculator_TestCase {
	
	static int board_height;
	static int board_width;
	static String[] user_input;
	static AdjacentSquareCalculator asc;
	
	@BeforeClass public static void initializeObjects()
	{
		board_height = 3;
		board_width = 5;
		
		user_input = new String[board_height];
		user_input[0] = "**...";
		user_input[1] = ".....";
		user_input[2] = ".*...";
		
		asc = new AdjacentSquareCalculator(board_height, board_width, user_input);
		
	}
		

	@Test
	public void test_isSafeSquare()
	{
	
		assertTrue(asc.isSafeSquare(0, 2));
		assertTrue(asc.isSafeSquare(0, 3));
		assertTrue(asc.isSafeSquare(0, 4));
		
		//Line 2 tests
		assertTrue(asc.isSafeSquare(1, 0));
		assertTrue(asc.isSafeSquare(1, 1));
		assertTrue(asc.isSafeSquare(1, 2));
		assertTrue(asc.isSafeSquare(1, 3));
		assertTrue(asc.isSafeSquare(1, 4));
		
		//Line 3 tests
		assertTrue(asc.isSafeSquare(2, 0));
		assertTrue(asc.isSafeSquare(2, 2));
		assertTrue(asc.isSafeSquare(2, 3));
		assertTrue(asc.isSafeSquare(2, 4));
		
		
	}
	
	@Test
	public void test_isMineSquare()
	{
		assertTrue(asc.isMineSquare(0, 0));
		assertTrue(asc.isMineSquare(0, 1));
		assertTrue(asc.isMineSquare(2, 1));
	}
	
	@Test
	public void test_calculate_safe_square_number() 
	{
	
		//Line 1 tests
		
		assertEquals(1, asc.calculateSafeSquareNumber(0, 2));
		assertEquals(0, asc.calculateSafeSquareNumber(0, 3));
		assertEquals(0, asc.calculateSafeSquareNumber(0, 4));
		
		//Line 2 tests
		assertEquals(3, asc.calculateSafeSquareNumber(1, 0));
		assertEquals(3, asc.calculateSafeSquareNumber(1, 1));
		assertEquals(2, asc.calculateSafeSquareNumber(1, 2));
		assertEquals(0, asc.calculateSafeSquareNumber(1, 3));
		assertEquals(0, asc.calculateSafeSquareNumber(1, 4));
		
		//Line 3 tests
		assertEquals(1, asc.calculateSafeSquareNumber(2, 0));
		assertEquals(1, asc.calculateSafeSquareNumber(2, 2));
		assertEquals(0, asc.calculateSafeSquareNumber(2, 3));
		assertEquals(0, asc.calculateSafeSquareNumber(2, 4));
	}
	
	
}
