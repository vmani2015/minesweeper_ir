package unit_tests;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import main.LaunchMinesweeperIR;

public class LaunchMinesweeperIR_TestCase {

	@Rule
	public final SystemOutRule sor = new SystemOutRule();
		
	@Test
	public void main_test_1argument()
	{
		String args[] = {"2"};
		//First enable the system log
		sor.enableLog();
		//Now launch the program
		LaunchMinesweeperIR.main(args);		
		//Write the assertion statement
		assertEquals(sor.getLog(),"Error: Please enter 2 arguments\nFormat: Number of Lines Number of Columns\n");
		//Now we clear the log
		sor.clearLog();
	}
		
	@Test
	public void main_test_3arguments()
	{
		String args[] = {"2","2","3"};
		//First enable the system log
		sor.enableLog();
		//Now launch the program
		LaunchMinesweeperIR.main(args);		
		//Write the assertion statement
		assertEquals(sor.getLog(),"Error: Please enter 2 arguments\nFormat: Number of Lines Number of Columns\n");
		//Now we clear the log
		sor.clearLog();
	}

}
