package unit_tests;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

import model.AdjacentSquareCalculator;
import model.GameLogic;

public class GameLogic_TestCase {
	
	
	static int board_height;
	static int board_width;
	static String[] user_input;
	static AdjacentSquareCalculator asc;
	static GameLogic gl;
	
	@BeforeClass public static void initializeObjects()
	{
		board_height = 3;
		board_width = 5;
		
		user_input = new String[board_height];
		user_input[0] = "**...";
		user_input[1] = ".....";
		user_input[2] = ".*...";
		
		asc = new AdjacentSquareCalculator(board_height, board_width, user_input);
		gl = new GameLogic(asc);
		
	}

	@Rule
	public final SystemOutRule sor = new SystemOutRule();
	

	@Test
	public void test_setAllSafeSquareNumbers()
	{
		//Run the methods
		String[] output = new String[board_height];
		output[0] = "**100";
		output[1] = "33200";
		output[2] = "1*100";
		
		assertArrayEquals(output, gl.setAllSafeSquareNumbers());
	}
	

}
