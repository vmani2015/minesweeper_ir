package view;

import java.util.Scanner;
import java.util.regex.Pattern;

public class UserInput {
	

	private String[] user_board_input;
	private int board_width;
	private int board_height;
	private Scanner input_reader;
	private Pattern regex_pattern;

	public UserInput()
	{
		
		input_reader = new Scanner(System.in);
		board_height = 0;
		board_width = 0;
		regex_pattern = Pattern.compile("[^\\*\\.]");
	}
	
	private String validate_user_board_line(int current_line_number)
	{
		String temp_line = "";
		boolean temp_line_valid = false;
		
		while (temp_line_valid == false)
		{
			//First we ask the user for the line
			temp_line = input_reader.nextLine();
			
			//Then we make sure that the length is correct
			if (temp_line.length() != board_width)
			{
				if (temp_line.length() < board_width)
				{
					//System.out.println("Error; The line width of line "+current_line_number+" is not "+board_width);
					System.out.println("Error; There are less than "+board_width+" chracters in line "+current_line_number+".");
				}
				else
				{
					System.out.println("Error; There are more than "+board_width+" chracters in line "+current_line_number+".");
				}
				
				System.out.println("Please enter line "+current_line_number+" again:");
			}
			//Then we need to make sure that the line
			//contains only valid characters
			else if (regex_pattern.matcher(temp_line).find())
			{
				System.out.println("Error; One or more characters in line "+current_line_number+" is not valid.");
				System.out.println("The only valid characters are (.) and (*)");
				System.out.println("Please enter line "+current_line_number+" again:");
			}
			else
			{
				temp_line_valid = true;
				break;
			}
		}
		
		
		
		return temp_line;
	}
	
	public void validate_user_board()
	{
//		System.out.println("Please enter gameboard:");
		String current_line = "";
		int current_line_counter = 1;
		
		while (current_line_counter <= board_height)
		{
			System.out.println("Please enter line "+current_line_counter+" of gameboard:");
			
			current_line = validate_user_board_line(current_line_counter);
			
			user_board_input[current_line_counter - 1] = current_line;
			
			current_line_counter++;
		}
		
		//Now print the user board
//		print_user_board();
		
		//We must also close the input scanner
		input_reader.close();
		
	}
	
	public void print_user_board()
	{
				System.out.println("The user board is:");
				for (int i = 0 ; i < user_board_input.length ; i++)
				{
					System.out.println(user_board_input[i]);
				}
				System.out.println("----------------------");
	}
	
	public void validate_board_dimensions(String[] args)
	{
			boolean both_valid = false;
			
			try
			{
				board_height = Integer.parseInt(args[0]);
				board_width = Integer.parseInt(args[1]);
				
				if (board_height <= 0 || board_width <= 0 || board_width >= 100)
				{
					throw new Exception();
				}
			}
			catch(Exception e)
			{
				//This code is there so that the user has to enter both the 
				//width and the height again.
				board_height = 0;
				board_width = 0;
				System.out.println("One or more of the arguments passed in was invalid.");
				System.out.println("Please enter both dimensions again.");
				while (both_valid == false)
				{
					
					try
					{
						if (board_height <= 0)
						{
							System.out.println("Please enter a valid board height:");
							board_height = input_reader.nextInt();
						}
						
						else if (board_width <= 0 || board_width >= 100)
						{
							System.out.println("Please enter a valid board width:");
							board_width = input_reader.nextInt();
						}
						
						if (board_height > 0 && board_width > 0 && board_width <100)
						{
							input_reader.nextLine();
							both_valid = true;
							break;
						}
						
					}
					catch(Exception ex)
					{
						System.out.println("Error");
						input_reader.nextLine();
					}
				}		
			}
			
//			System.out.println("Board Dimensions valid");
			System.out.println("Board Dimensions are : "+board_height+"*"+board_width);
			//Now we declare the user_board
			user_board_input = new String[board_height];
			
		
			
	}
	
	
	public String[] getUser_board_input() {
		return user_board_input;
	}

	public int getBoard_width() {
		return board_width;
	}

	public int getBoard_height() {
		return board_height;
	}
		
	
	
	
}
