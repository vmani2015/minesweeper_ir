package model;

public class AdjacentSquareCalculator {
	
	private int current_x;
	private int current_y;
	private int board_width;
	private int board_length;
	private String[] user_input;
	
	public String[] getUser_input() {
		return user_input;
	}

	public AdjacentSquareCalculator(int pboard_length,int pboard_width,String[] p_user_input)
	{
		user_input = p_user_input;
		board_length = pboard_length;
		board_width = pboard_width;
		current_x = 0;
		current_y = 0;
	}
	
	private boolean isCoordinateValid(int x, int y)
	{
		if ( x < 0 || x > board_width-1)
		{
			return false;
		}
		else if ( y < 0 || y > board_length-1)
		{
			return false;
		}
		
		return true;
	}
	
	private int incrementNumberifMineSquare()
	{
		if (isMineSquare(current_y, current_x))
		{
			return 1;
		}
		
		return 0;
	}
	
	//Top
	private int getTop(int x,int y)
	{
		if (isCoordinateValid(x, y-1))
		{
			current_x = x;
			current_y = y-1;
			
			return incrementNumberifMineSquare();
			
		}
		
		return 0;
	}
	
	//Bottom
	private int getBottom(int x,int y)
	{
		if (isCoordinateValid(x, y+1))
		{	
			current_x = x;
			current_y = y+1;
			
			return incrementNumberifMineSquare();
		}
		
		return 0;
	}
	
	
//	Left
		private int getLeft(int x,int y)
		{
			if (isCoordinateValid(x-1, y))
			{
				current_x = x-1;
				current_y = y;

				
				return incrementNumberifMineSquare();
			}
			
			return 0;
		}
	
	//Right
		
		private int getRight(int x,int y)
		{
			if (isCoordinateValid(x+1, y))
			{
				current_x = x+1;
				current_y = y;

				
				return incrementNumberifMineSquare();
			}
			
			return 0;
		}
	
	
	//Top Right
		
		private int getTopRight(int x,int y)
		{
			if (isCoordinateValid(x+1, y-1))
			{
				current_x = x+1;
				current_y = y-1;

				return incrementNumberifMineSquare();
			}
			
			return 0;
		}
		
		
		
	//Top Left
		
		private int getTopLeft(int x,int y)
		{
			if (isCoordinateValid(x-1, y-1))
			{
				current_x = x-1;
				current_y = y-1;

				return incrementNumberifMineSquare();
			}
			
			return 0;
		}
		
		
		
	//Bottom Left
		private int getBottomLeft(int x,int y)
		{
			if (isCoordinateValid(x-1, y+1))
			{
				current_x = x-1;
				current_y = y+1;

				return incrementNumberifMineSquare();
			}
			
			return 0;
		}
		
		
	//Bottom Right
		
		
		private int getBottomRight(int x,int y)
		{
			if (isCoordinateValid(x+1, y+1))
			{
				current_x = x+1;
				current_y = y+1;

				return incrementNumberifMineSquare();
			}
			
			return 0;
		}
		
		
		public int calculateSafeSquareNumber(int y,int x)
		{
			
			int total = 
					getBottom(x, y) + 
					getBottomLeft(x, y) + 
					getBottomRight(x, y) + 
					getTop(x, y)+ 
					getTopLeft(x, y) + 
					getTopRight(x, y) + 
					getLeft(x, y) + 
					getRight(x, y);
			
			return total;
		}
		
		public boolean isMineSquare(int y,int x)
		{
			if (user_input[y].substring(x, x+1).equalsIgnoreCase("*") )
			{
				return true;
			}
			
			return false;
		}
		
		public boolean isSafeSquare(int y,int x)
		{
			if (user_input[y].substring(x, x+1).equalsIgnoreCase(".") )
			{
				return true;
			}
			
			return false;
		}
		
		public boolean isNumberedSquare(int y, int x)
		{
			if (Character.isDigit(user_input[y].substring(x, x+1).charAt(0)))
			{
				return true;
			}
			
			return false;
			
		}
		
		
}
