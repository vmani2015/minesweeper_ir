package model;


public class GameLogic {

	private int board_length;
	private int board_width;
	private String[] user_input;
	private AdjacentSquareCalculator asc;
	
	
	public GameLogic(AdjacentSquareCalculator p_asc)
	{
		asc = p_asc;
		user_input = p_asc.getUser_input();
		board_length = user_input.length;
		board_width = user_input[0].length();
	}
			
		public String[] setAllSafeSquareNumbers()
		{
			int safe_square_number = 0;
			String current_string;
			
			//This is going through the columns first
			for (int i = 0 ; i < board_length ; i++)
			{
				current_string = "";
				//Then we go through each character of a line
				for (int j = 0 ; j < board_width ; j++)
				{
				
					//Only do this if it is a safe square
					if (asc.isSafeSquare(i, j))
					{
						safe_square_number = asc.calculateSafeSquareNumber(i,j);
						current_string = current_string + Integer.toString(safe_square_number);	
					}
					else
					{
						current_string = current_string + "*";
					}	
				}
				user_input[i] = current_string;
			}
			
			return user_input;
			
		}
		
		
		
		public void printOutput()
		{
			for (int i = 0 ; i < board_length; i++)
			{
				System.out.println(user_input[i]);				
			}
		}
		
}
